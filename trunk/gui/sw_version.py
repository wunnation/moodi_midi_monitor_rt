#from PySide2 import QtCore, QtGui, QtWidgets
from PyQt4 import QtGui, QtCore
QtWidgets = QtGui # For Qt4 compatibility with code also compatible with Qt5

MOODI_MIDI_MONITOR_VERSION = 107

class VersioningWidgetClass(QtWidgets.QWidget):
    def __init__(self, parent=None):
        self.firmware = QtWidgets.QLabel('0.00')
        self.software = QtWidgets.QLabel('0.00')
        self.firmware_LABEL = QtWidgets.QLabel('firmware')
        self.software_LABEL = QtWidgets.QLabel('version')
        self.align_labels()
        self.layout = QtWidgets.QGridLayout()
        self.layout.addWidget(self.software_LABEL,0,0)
        self.layout.addWidget(self.software,0,1)

    def align_labels(self):
        self.firmware.setAlignment(QtCore.Qt.AlignLeft | QtCore.Qt.AlignBottom)
        self.firmware_LABEL.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignBottom)
        self.software.setAlignment(QtCore.Qt.AlignLeft | QtCore.Qt.AlignBottom)
        self.software_LABEL.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignBottom)

    def integer_to_version_string(self, firmware_version):
        firmware_string = str(firmware_version)
        if len(firmware_string) < 3:
            firmware_string = '0.' + firmware_string # pad with '0.'
        else:
            firmware_string = firmware_string[0:len(firmware_string)-2] + '.' + firmware_string[len(firmware_string)-2:len(firmware_string)]
        return firmware_string

