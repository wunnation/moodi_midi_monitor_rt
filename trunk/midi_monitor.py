#!/usr/bin/env python

#include Standard Python Libs
import sys
import time
#include Gui
#from PySide2 import QtCore, QtGui, QtWidgets
from PyQt4 import QtCore, QtGui
QtWidgets = QtGui # For Qt4 compatibility with code also compatible with Qt5

#include Custom MIDI Backends
from midi.rtmidi_class import MIDISystemRt

# Include All Widgets
from gui_midi_monitor import MIDIMonitor_Window, MIDISend_Window, MidiDeviceSelectWidget, MIDIThrough_Window

# Include gui images
from gui.find_images import GetImagesDirectory

# Include versioning
from gui.sw_version import MOODI_MIDI_MONITOR_VERSION, VersioningWidgetClass

#import pyautogui

# Define Constants related to this main Window
APP_TITLE = "Moodi MIDI Monitor RT"
PIXMAP_DIR = GetImagesDirectory()

# ========= Main Window ==================================
# Place All Widgets in Main Window
# MIDI System is also created and destroyed in this class
# =========================================================
class ProjectMainWindow(QtWidgets.QMainWindow):
    def __init__(self, parent=None):
        # Initialize the Main Window
        super(ProjectMainWindow, self).__init__(parent)      # Calls the basic TextEdit Init Sequence
        self.auto_connect_string = "MODULE 01"
        self.setWindowIcon(QtGui.QIcon(PIXMAP_DIR + 'moodi_midi_monitor_logo_square.png'))
        self.debug_output = False
        self.init_gui()
        self.init_midi_sys()
        self.start_midi_session()

    def endProgram(self):  # Runs Just before application Exits
        # Stop all Qtimers
        self.timer_midimon.stop()
        time.sleep(.005)
        #self.EndMIDISession()


    def init_gui(self):
        self.init_docks()
        self.init_central_widget()
        self.fixed_size = [650,650]
        rect = QtCore.QRect(25,25,25+self.fixed_size[0],25+self.fixed_size[1])
        self.setGeometry(rect)
        # Versioning: Set Software Version
        self.update_software_version()

    def init_central_widget(self): # Must be called in __init__
        # Set the Central Widget in the Main Window
        self.setCentralWidget(self.midi_in_group)
    def init_docks(self): # Must be called in __init__
        # Declare all widgets
        # - Midi Rx
        self.midi_in_select = MidiDeviceSelectWidget()
        self.midi_monitor_win = MIDIMonitor_Window() # non-dock
        self.midi_through_checkbox = MIDIThrough_Window()
        # -- GroupBox
        self.midi_in_group = QtWidgets.QGroupBox("MIDI In (Receive Messages From)")
        self.midi_in_group_layout = QtWidgets.QVBoxLayout()
        self.midi_in_group_layout.addWidget(self.midi_in_select)
        self.midi_in_group_layout.addWidget(self.midi_through_checkbox)
        self.midi_in_group_layout.addWidget(self.midi_monitor_win)
        self.midi_in_group.setLayout(self.midi_in_group_layout)
        # - Midi Tx
        self.midi_out_select = MidiDeviceSelectWidget()
        self.midi_send_tab = MIDISend_Window() # non-dock
        self.midi_out_group = QtWidgets.QGroupBox("MIDI Out (Send Messages To)")
        self.midi_out_group_layout = QtWidgets.QVBoxLayout()
        self.midi_out_group_layout.addWidget(self.midi_out_select)
        self.midi_out_group_layout.addWidget(self.midi_send_tab)
        self.midi_out_group.setLayout(self.midi_out_group_layout)

        # Set Up Docks and Main Window
        # ----- Midi Port Select Widget (dock-Top)----------------
        dock_top = QtWidgets.QDockWidget(self)
        dock_top.setWidget(self.midi_out_group)
        dock_top.setFeatures(QtWidgets.QDockWidget.NoDockWidgetFeatures)
        dock_top.setFixedHeight(175) # Fix the Height of the MIDI Selection Widget so it is not expanded
        self.addDockWidget(QtCore.Qt.TopDockWidgetArea, dock_top)

        # -- Callbacks
        self.midi_in_select.port_select.activated.connect(self.midi_input_selected) # Callback: Device Changed
        self.midi_out_select.port_select.activated.connect(self.midi_output_selected) # Callback: Device Changed
        self.midi_through_checkbox.checkbox.stateChanged.connect(self.midi_through_changed)

        # - Midi filtering
        self.midi_through_enabled = False
        self.init_midi_monitor_message_filter()

    def update_software_version(self):
        version_string = self.midi_monitor_win.versioning.integer_to_version_string(MOODI_MIDI_MONITOR_VERSION)
        self.midi_monitor_win.versioning.software.setText(version_string)
        self.setWindowTitle(APP_TITLE + " v" + version_string)

    # ============== MIDI Session =========================
    def init_midi_sys(self):
        self.midi_sys = False
    
    def start_midi_session(self):
        self.midi_sys = MIDISystemRt() # Initialize MIDI Systemx

        #self.midi_sys.add_slot_midi_in()
        #self.midi_sys.add_slot_midi_out()
        self.midi_monitor_win.text_out.append("midi session re-started. \n")
        self.midi_monitor_win.text_out.moveCursor(QtGui.QTextCursor.End) # Scroll to end of MIDI Monitor

        # initialize midi monitor gui timer
        self.timer_midimon = QtCore.QTimer(self)                 # set up a timer
        self.timer_midimon.timeout.connect(self.midi_monitor_display_timer_callback)   # when the timer goes off, run this function
        self.MIDI_MONITOR_LATENCY = 20                               # 1 (90% Processing), 2 (45% Processing), 3 (15% Processing), 5 (8%), 10 (negligeable) 
        self.timer_midimon.start(self.MIDI_MONITOR_LATENCY)   

        # initialize automatic connection gui timer
        self.timer_midicon = QtCore.QTimer(self)                 # set up a timer
        self.timer_midicon.timeout.connect(self.midi_device_connection_timer_callback)   # when the timer goes off, run this function
        self.MIDI_CONNECTION_TIMER_LATENCY = 500         # 1 (90% Processing), 2 (45% Processing), 3 (15% Processing), 5 (8%), 10 (negligeable) 
        self.timer_midicon.start(self.MIDI_CONNECTION_TIMER_LATENCY)   

        # initialize gui portion
        self.refresh_midi_input_select()
        self.refresh_midi_output_select()

        # initialize automatic connection (if enabled)
        self.auto_connect_to_midi_device()

    def end_midi_session(self):
        pass
    # ============== MIDI Session =====================END=

    # == MIDI Device Selection GUI Combobox =================
    def refresh_midi_input_select(self):
        if not self.midi_sys:
            return False

        connection_id = 0
        midi_inputs = len(self.midi_sys.midi_in_port_names)
        midi_input_id = self.midi_sys.midi_input_indexes[connection_id]
        # Clear the midi port select List
        self.midi_in_select.port_select.clear()
        # Add an Entry for Each Port Pair
        for in_num in range(midi_inputs):
            entry_text = str(in_num) + '. ' + self.midi_sys.midi_in_port_names[in_num]
            self.midi_in_select.port_select.addItem(entry_text)
        # Add an Entry for 'Not Connected'
        self.midi_in_select.port_select.addItem("< Disconnected >")
        # Set the Combobox to show to the currently connected midi port
        if midi_input_id < midi_inputs: #!revision: port validation () and self.midi_in: # Case: Valid MIDI Port is connected
            self.midi_in_select.port_select.setCurrentIndex(midi_input_id) 
        else:
            self.midi_in_select.port_select.setCurrentIndex(midi_inputs) 

    def refresh_midi_output_select(self):
        if not self.midi_sys:
            return False

        connection_id = 0
        midi_outputs = len(self.midi_sys.midi_out_port_names)
        midi_output_id = self.midi_sys.midi_output_indexes[connection_id]
        # Clear the midi port select List
        self.midi_out_select.port_select.clear()
        # Add an Entry for Each Port Pair
        for out_num in range(midi_outputs):
            entry_text = str(out_num) + '. ' + self.midi_sys.midi_out_port_names[out_num]
            self.midi_out_select.port_select.addItem(entry_text)
        # Add an Entry for 'Not Connected'
        self.midi_out_select.port_select.addItem("< Disconnected >")
        # Set the Combobox to show to the currently connected midi port
        if midi_output_id < midi_outputs: #!revision: port validation () and self.midi_in: # Case: Valid MIDI Port is connected
            self.midi_out_select.port_select.setCurrentIndex(midi_output_id) 
        else:
            self.midi_out_select.port_select.setCurrentIndex(midi_outputs)   

    def midi_output_selected(self): # Gui Selection: Connect to MIDI Port
        self.midi_sys.close_midi_out(0)
        midi_out_num = self.midi_out_select.port_select.currentIndex()
        self.midi_sys.open_midi_out(0, midi_out_num)

    def midi_input_selected(self, event, data=None): # Gui Selection: Connect to MIDI Port
        self.midi_sys.close_midi_in(0)
        midi_in_num = self.midi_in_select.port_select.currentIndex()
        self.midi_sys.open_midi_in(0, midi_in_num, callback=self.midi_input_callback)

    def midi_through_changed(self, state):
        if state == QtCore.Qt.Checked:
            self.midi_through_enabled = True
            print('midi through Checked')
        else:
            print('midi through Unchecked')
            self.midi_through_enabled = False

    def midi_monitor_display_timer_callback(self): # Update Midi Monitor Display of Messages
        self.midi_monitor_win.displayBackloggedMidiMessages()

    def auto_connect_to_midi_device(self): # Enable and Start Auto connection to a particular device
        this_string = self.auto_connect_string
        if this_string:
            input_id, output_id = self.midi_sys.enable_auto_connect_to_midi_device(0, self.auto_connect_string, self.midi_input_callback)
            print ("auto connect to", this_string)
            self.midi_out_select.port_select.setCurrentIndex(output_id)
            self.midi_in_select.port_select.setCurrentIndex(input_id)
        else:
            print ("auto connect disabled.")

    def midi_device_connection_timer_callback(self): # Check Connection Status of Auto Connected Device(s)
        # Check Updates with MIDI Backend
        #print "update auto connect..."
        input_id, output_id, input_updated, output_updated = self.midi_sys.update_auto_connect(0, self.midi_input_callback)

        if input_updated: # If Input Updated, update the GUI
            self.refresh_midi_input_select()
            self.midi_in_select.port_select.setCurrentIndex(input_id)

        if output_updated: # If Output Updataed, update the GUI
            self.refresh_midi_output_select()
            self.midi_out_select.port_select.setCurrentIndex(output_id)

    def midi_input_callback(self, event, data=None):
        #print "MIDI Input Callback"
        # Patch Message through to MIDI Through Port
        if self.midi_through_enabled:
            if len(event) > 0:
                self.midi_sys.send_midi_message(0, event[0])
        # Print to Display Window
        if len(event) >= 2:
            self.midi_monitor_win.displayMidiMessage(event[0], event[1], head_str='Rx')
        else:
            self.midi_monitor_win.displayMidiMessage(event, time.time(), head_str='Rx')
    # == MIDI Device Selection GUI Combobox =============END=

    # ============== MIDI Display Filtering Init/ GUI Callbacks ==============
    def init_midi_monitor_message_filter(self):
        self.midi_channel_filter = range(16)
        self.midi_type_filter = [] # Empty List means no filtering
        self.midi_num_filter_min = 0
        self.midi_num_filter_max = 127
        self.midi_num_filter = range(self.midi_num_filter_min, self.midi_num_filter_max+1) # All Numbers 0-127 (min to max)
        # - Callbacks
        self.midi_monitor_win.buttons.midi_filter_channel.currentIndexChanged.connect(self.update_midi_channel_filter)  
        self.midi_monitor_win.buttons.midi_filter_type.currentIndexChanged.connect(self.update_midi_type_filter)  
        self.midi_monitor_win.buttons.midi_filter_num_min.valueChanged.connect(self.update_midi_num_filter_min)  
        self.midi_monitor_win.buttons.midi_filter_num_max.valueChanged.connect(self.update_midi_num_filter_max)  
        self.midi_send_tab.button.clicked.connect(self.send_midi_message_from_gui)
        self.midi_send_tab.text_box.returnPressed.connect(self.send_midi_message_from_gui_keypress)

    def update_midi_channel_filter(self, filter_channel_index):
        if filter_channel_index >= 16:
            print("Filter Channel All")
            self.midi_channel_filter = range(16) # Empty List means no filtering
        else:
            self.midi_channel_filter = [filter_channel_index]
            print("Filter Channel", filter_channel_index)
        pass
    def update_midi_type_filter(self, filter_type_index):
        if filter_type_index == 0:
            print("Filter Type All")
            self.midi_type_filter = [] # Empty List means no filtering
            self.midi_monitor_win.buttons.midi_filter_num_min.setDisabled(True)
            self.midi_monitor_win.buttons.midi_filter_num_max.setDisabled(True)
            self.midi_monitor_win.buttons.midi_filter_channel.setDisabled(True)
        elif filter_type_index == 1: # case Notes Only
            print("Filter Type Notes")
            self.midi_type_filter = [0x80,0x90] #range(0x80,0xA0) # Note on and Note off messages (all channels)
            self.midi_monitor_win.buttons.midi_filter_num_min.setDisabled(False)
            self.midi_monitor_win.buttons.midi_filter_num_max.setDisabled(False)
            self.midi_monitor_win.buttons.midi_filter_channel.setDisabled(False)
        elif filter_type_index == 2:
            print("Filter Type CCs")
            self.midi_type_filter = [0xB0] #range(0xB0,0xC0) # Note on and Note off messages (all channels)
            self.midi_monitor_win.buttons.midi_filter_num_min.setDisabled(False)
            self.midi_monitor_win.buttons.midi_filter_num_max.setDisabled(False)
            self.midi_monitor_win.buttons.midi_filter_channel.setDisabled(False)
            pass
        elif filter_type_index == 3:
            print("Filter Type Notes+CCs")
            self.midi_type_filter = [0x80, 0x90, 0xB0] #range(0xB0,0xC0) # Note on and Note off messages (all channels)
            self.midi_monitor_win.buttons.midi_filter_num_min.setDisabled(False)
            self.midi_monitor_win.buttons.midi_filter_num_max.setDisabled(False)
            self.midi_monitor_win.buttons.midi_filter_channel.setDisabled(False)
            pass

        else:
            print("Filter Type UNKNOWN", filter_type_index)
        pass
    def update_midi_num_filter_min(self, this_value):
        self.midi_num_filter_min = this_value
        self.midi_num_filter = range(self.midi_num_filter_min, self.midi_num_filter_max+1) # All Numbers 0-127 (min to max)
    def update_midi_num_filter_max(self, this_value):
        self.midi_num_filter_max = this_value
        self.midi_num_filter = range(self.midi_num_filter_min, self.midi_num_filter_max+1) # All Numbers 0-127 (min to max)
    # ============== MIDI Display Filtering Init/ GUI Callbacks ==========END=

    # ============== MIDI Send GUI Callbacks ===================
    def send_midi_message_from_gui_keypress(self):
        print ("send midi - keypress")
        self.send_midi_message_from_gui(0)

    def send_midi_message_from_gui(self, this_value): # !review: this value is unused
        print ("send midi")
        text_value = self.midi_send_tab.text_box.text()
        midi_message = self.midi_send_tab.text_box.parse_data_line(text_value)
        if len(midi_message):
            self.midi_sys.send_midi_message(0, midi_message)
            self.midi_monitor_win.displayMidiMessage(midi_message, time.time(), head_str='Sent')

    # ============== MIDI Send GUI Callbacks  ===============END=

def main_program():
    app = QtWidgets.QApplication([APP_TITLE])
    mw = ProjectMainWindow()
    mw.show()
    app.aboutToQuit.connect(mw.endProgram)
    sys.exit(app.exec_())
    #app.deleteLater()	 

if __name__ == "__main__":
    main_program()

