#
# midi_h.py
#
import sys

#-------Message Types-------#
MessageNames=("NoteOff","NoteOn","KeyPressure","CC","ProgChange","ChanPressure","PitchBend","SysEx")
MessageTypes=(  0x80,    0x90,        0xA0,    0xB0,    0xC0,         0xD0,         0xE0,    0xF0)
MessageDataSize=(3,        3,           3,      3,        2,            2,            3,     0xF7)
MIDI_MIN_MESSAGE_TYPE	=0x08 # MIDI Messages must begin with an 0x80 flag, if they do not, then 
#MIDI_SYSTEM_MESSAGETYPE	=0x0F # Sysex/System Realtime/ etc...
MIDI_NOTE_OFF		=0x80
MIDI_NOTE_ON		=0x90
MIDI_KEYPRESSURE	=0xA0
MIDI_CC			=0xB0
MIDI_PROGCHANGE		=0xC0
MIDI_CHANPRESSURE	=0xD0
MIDI_PITCHBEND		=0xE0
MIDI_SYSEX		=0xF0
MIDI_MAX_VALUE		=0x7F
MIDI_MAX_VELOCITY	=0x7F
MIDI_MESSAGE_TYPE_MASK  =0xF0
MIDI_CHANNEL_MASK 	=0xF0
#-------Message Types-------#

#-----Common  Messages------#
#CommonMessageNames()
#CommonMessageTypes()
MIDI_TIME_CODE		=0xF1
MIDI_SONG_POS		=0xF2
MIDI_SONG_SEL		=0xF3
#MIDI_undefined1	=0xF4
#MIDI_undefined2	=0xF5
MIDI_TUNE		    =0xF6
MIDI_SYSEX_END		=0xF7
#----Common  Messages-------#

#----Special CC's-----------#
#SpecialCCNames()
#SpecialCCTypes()
MIDI_ALL_SOUND_OFF	=120
MIDI_RESET_CONTROLS	=121
MIDI_LOCAL_CONTROL	=122
MIDI_ALL_NOTES_OFF	=123
MIDI_OMNI_OFF		=124
MIDI_OMNI_ON		=125
MIDI_MONO_ON		=126
MIDI_POLY_ON		=127
#----Special CC's-----------#

#----SystemRealTimeMessages--#
MIDI_TIME_CLOCK		=0xF8
#MIDI_undefined3	=0xF9
MIDI_START		=0xFA
MIDI_CONTINUE		=0xFB
MIDI_STOP		=0xFC
#MIDI_undefined4	=0xFD
MIDI_ACTIVE		=0xFE
MIDI_RESET		=0xFF
#----SystemRealTimeMessages--#


MIDI_CONTROL_MAX	=116
MIDI_SYS_EX_MID_EXT	=0x00
MIDI_SYS_EX_PRIVATE	=0x7D
MIDI_SYS_EX_NON_REAL_TIME=0x7E
MIDI_SYS_EX_REAL_TIME	=0x7F
MIDI_SYS_EX_OMNI	=0x7F
MIDI_NRT_MTC		=0x04
MIDI_MTC_SPECIAL	=0x00
MIDI_NRT_INQUIRY	=0x06
MIDI_INQ_REQUEST	=0x01
MIDI_INQ_REPLY		=0x02
MIDI_RT_TIME_CODE	=0x01

INPUT = 0
OUTPUT = 1

MIDI_SYSEX_MIN_SIZE	=0x04
