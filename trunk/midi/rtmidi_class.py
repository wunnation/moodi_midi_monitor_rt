# ==== pypm_class.py =======
# = pypm_class is a midi instance that can easily be created and deleted
import rtmidi

def GetMidiPortNames(midi_in, midi_out):
    #midi_out = rtmidi.MidiOut()
    #midi_in = rtmidi.MidiIn()
    out_port_names = midi_out.get_ports()
    in_port_names = midi_in.get_ports()
    return in_port_names, out_port_names

def GetMidiPortPairs(midi_in_port_names, midi_out_port_names):
    midi_pairs = []
    midi_match_found = [False]*len(midi_out_port_names)
    for port_in in range(len(midi_in_port_names)):   # See if the names of any input ports match those of any output ports
        for port_out in range(len(midi_out_port_names)):
            if (midi_in_port_names[port_in] == midi_out_port_names[port_out]) and not midi_match_found[port_out]: # [0] = Driver Name [1] = Port Name
                midi_pairs.append([port_in, port_out, midi_out_port_names[port_out]])
                midi_match_found[port_out] = True
                break # only allow one match per input
    print ("MIDI Pairs", midi_pairs)
    return midi_pairs

class MIDISystemRt():
    # Initialize MIDI System
    def __init__(self, testing_enabled = False): # Routine that is run when the item is created
        # Setup MIDI Connection Slots
        self.midi_inputs = []
        self.midi_input_indexes = []
        self.midi_input_reconnect_strings = []

        self.midi_outputs = []
        self.midi_output_indexes = []
        self.midi_output_reconnect_strings = []
        self.MIDI_INDEX_NA = 0xFFFF# Not Assigned
        self.testing_enabled = testing_enabled

        # - Auto Connect
        self.auto_connect_string = "" # !error: deprecate
        self.midi_in_auto_connect_strings = []
        self.midi_out_auto_connect_strings = []

        self.add_slot_midi_in()
        self.add_slot_midi_out()
        # - Test Initialization of Connection slots
        if self.testing_enabled:
            DEVICE_CONNECTION_SLOTS = 2
            for this_connection in range(DEVICE_CONNECTION_SLOTS):
                self.add_slot_midi_in()
                self.assign_midi_in_callback(this_connection, self.default_midi_callback)
                self.add_slot_midi_out()

        # Discover MIDI Ports that are accessible by this application
        self.midi_in_port_names, self.midi_out_port_names = GetMidiPortNames(self.midi_inputs[0], self.midi_outputs[0])
        self.MidiPairs = GetMidiPortPairs(self.midi_in_port_names, self.midi_out_port_names)  # Get List of Matching MIDI Ports

    def reboot_midi_system(self): # close all connections 
        pass

    # Add MIDI Connnection Slots (So you can manage more than one connection with this class)
    def add_slot_midi_out(self): 
        self.midi_outputs.append(rtmidi.MidiOut())
        self.midi_output_indexes.append(self.MIDI_INDEX_NA)
        self.midi_out_auto_connect_strings.append("")
        return len (self.midi_outputs)

    def add_slot_midi_in(self): 
        self.midi_inputs.append(rtmidi.MidiIn())
        self.midi_inputs[-1].ignore_types(sysex=False)
        self.midi_input_indexes.append(self.MIDI_INDEX_NA)
        self.midi_in_auto_connect_strings.append("")
        return len(self.midi_inputs)

    # Open/Close MIDI Port in a Connection Slot 
    def close_midi_in(self, in_connection_id):
        self.midi_inputs[in_connection_id].close_port()
        pass
    def close_midi_out(self, out_connection_id):
        self.midi_outputs[out_connection_id].close_port()
        pass

    def open_midi_out(self, out_connection_id, out_index):
        if out_index == self.MIDI_INDEX_NA:
            print ("failed to open midi out (index)")
            return
        status = self.midi_outputs[out_connection_id].open_port(out_index, "moodi_mm"+str(out_connection_id))
        print("open midi output:" + str(status))
        self.midi_output_indexes[out_connection_id] = out_index
        self.midi_output_reconnect_strings.append("")

    def open_midi_in(self, in_connection_id, in_index, callback = None):
        if in_index == self.MIDI_INDEX_NA:
            print ("failed to open midi in (index)")
            return
        status = self.midi_inputs[in_connection_id].open_port(in_index, "moodi_mm"+str(in_connection_id))
        #status = self.midi_inputs[in_connection_id].open_port(in_index)
        print ("open midi in:", status)
        self.midi_input_indexes[in_connection_id] = in_index
        self.midi_input_reconnect_strings.append("")

        if callback:
            self.assign_midi_in_callback(in_connection_id, callback)

    # MIDI Port Callbacks
    def assign_midi_in_callback(self, connection_id, callback_function):
        print ("assign midi in callback", connection_id, callback_function)
        self.midi_inputs[connection_id].set_callback(callback_function)

    def default_midi_callback(self, event, data=None):
        if len(event) >= 2:
            print ("rx midi message:", event[0], " -> Time Since Last Message: ", event[1], "-> Data:", data)
        else:
            print ("rx unknown event structure:", event, data)

    def send_midi_message(self, connection_id, message):
        print ("send midi message")
        self.midi_outputs[connection_id].send_message(message)

    # Find MIDI Ports
    def refresh_midi_device_list(self): # check for changes
        last_midi_in_port_names = list(self.midi_in_port_names)
        last_midi_out_port_names = list(self.midi_out_port_names)
        self.midi_in_port_names, self.midi_out_port_names = GetMidiPortNames(self.midi_inputs[0], self.midi_outputs[0])
        changed_ports = self.identify_changed_ports(last_midi_in_port_names, last_midi_out_port_names)
        return changed_ports

    def find_midi_input_by_string(self, port_string): # Returns Single Integer
        for in_num in range(len(self.midi_in_port_names)):
            if port_string.lower() in self.midi_in_port_names[in_num].lower(): # !review: case insensitive (currently)
                return in_num
        return self.MIDI_INDEX_NA # 0xFF means not detected

    def find_midi_output_by_string(self, port_string): # Returns Single Integer
        for out_num in range(len(self.midi_out_port_names)):
            if port_string.lower() in self.midi_out_port_names[out_num].lower(): # !review: case insensitive (currently)
                return out_num
        return self.MIDI_INDEX_NA # 0xFF means not detected

    # Automatic Reconnection
    # - Combined
    def update_auto_connect(self, connection_id, midi_in_callback):
        print("update auto conn")
        input_updated = False
        output_updated = False
        # -- Update Device List
        [[dropped_in, dropped_out], [added_in, added_out]] = self.refresh_midi_device_list()
        # -- Inputs
        # for connection_id in range(self.midi_in_auto_connect_strings):
        this_string = self.midi_in_auto_connect_strings[connection_id]
        input_id = self.find_midi_input_by_string(this_string)
        if input_id in added_in:
            print ("-input just connected!")
            self.close_midi_in(connection_id)
            self.open_midi_in(connection_id, input_id, midi_in_callback)
            input_updated = True
        # -- Outputs
        this_string = self.midi_out_auto_connect_strings[connection_id]
        output_id = self.find_midi_output_by_string(this_string)
        if output_id in added_out:
            print ("-output just connected!")
            self.close_midi_out(connection_id)
            self.open_midi_out(connection_id, output_id)
            output_updated = True

        return input_id, output_id, input_updated, output_updated


    def enable_auto_connect_to_midi_device(self, connection_id, device_string, midi_input_callback):
        input_id = self.enable_midi_in_auto_connect_by_string(connection_id, device_string, midi_input_callback)
        output_id = self.enable_midi_out_auto_connect_by_string(connection_id, device_string)
        return input_id, output_id
    # - MIDI In
    def enable_midi_in_auto_connect_by_string(self, in_connection_id, device_string, midi_input_callback):
        self.midi_in_auto_connect_strings[in_connection_id] = device_string
        input_id = self.auto_connect_to_midi_in(in_connection_id, midi_input_callback)
        return input_id

    def disable_midi_in_auto_connect_by_string(self, in_connection_id):
        self.midi_in_auto_connect_strings[in_connection_id] = ""

    def auto_connect_to_midi_in(self, in_connection_id, midi_input_callback):
        device_string = self.midi_in_auto_connect_strings[in_connection_id]
        input_id = self.find_midi_input_by_string(device_string)
        self.open_midi_in(in_connection_id, input_id, callback=midi_input_callback)
        return input_id
    
    # - MIDI Out
    def enable_midi_out_auto_connect_by_string(self, out_connection_id, device_string):
        self.midi_out_auto_connect_strings[out_connection_id] = device_string
        output_id = self.auto_connect_to_midi_out(out_connection_id)
        return output_id

    def disable_midi_out_auto_connect_by_string(self, out_connection_id):
        self.midi_out_auto_connect_strings[out_connection_id] = ""

    def auto_connect_to_midi_out(self, out_connection_id):
        device_string = self.midi_out_auto_connect_strings[out_connection_id]
        output_id = self.find_midi_output_by_string(device_string)
        self.open_midi_out(out_connection_id, output_id)
        return output_id

    # - Look for changed Ports
    def identify_changed_ports(self, last_midi_in_port_names, last_midi_out_port_names):
        dropped_input_indexes = []
        dropped_output_indexes = []
        added_input_indexes = []
        added_output_indexes = []
        # Find changed midi_inputs
        if self.midi_in_port_names != last_midi_in_port_names:
            print ("Input Port Change(s) Detected!")#, last_midi_in_port_names, self.midi_in_port_names
            # Check for Dropped Ports
            for this_input in range(len(last_midi_in_port_names)):
                if not last_midi_in_port_names[this_input] in self.midi_in_port_names:
                    print ("- ", last_midi_in_port_names[this_input], "has left the building.")
                    dropped_input_indexes.append(this_input)
            # Check for Added Ports
            for this_input in range(len(self.midi_in_port_names)):
                if not self.midi_in_port_names[this_input] in last_midi_in_port_names:
                    print ("- ", self.midi_in_port_names[this_input], "has joined the party.")
                    added_input_indexes.append(this_input)
        else:
            pass
            # print "Input Ports were not changed."

        # find changed midi_outputs
        if self.midi_out_port_names != last_midi_out_port_names:
            print ("Output Port Change(s) Detected!")#, last_midi_out_port_names, self.midi_out_port_names
            # Check for Dropped Ports
            for this_output in range(len(last_midi_out_port_names)):
                if not last_midi_out_port_names[this_output] in self.midi_out_port_names:
                    print ("- ", last_midi_out_port_names[this_output], "has left the building.")
                    dropped_output_indexes.append(this_output)
            # Check for Added Ports
            for this_output in range(len(self.midi_out_port_names)):
                if not self.midi_out_port_names[this_output] in last_midi_out_port_names:
                    print ("- ", self.midi_out_port_names[this_output], "has joined the party.")
                    added_output_indexes.append(this_output)
        else:
            pass
            # print "output Ports were not changed."
            
        self.identify_dropped_connections(dropped_input_indexes, dropped_output_indexes, last_midi_in_port_names, last_midi_out_port_names)
        self.identify_reattached_connections(added_input_indexes, added_output_indexes)

        return [[dropped_input_indexes, dropped_output_indexes],[added_input_indexes, added_output_indexes]]


    # - Look for missing ports that effect active connection slots
    def identify_dropped_connections(self, dropped_input_indexes, dropped_output_indexes, last_midi_in_port_names, last_midi_out_port_names):
        for this_connection in range(len(self.midi_input_indexes)):
            this_index = self.midi_input_indexes[this_connection]
            if this_index in dropped_input_indexes:
                print ("- Dropped Input Connection", this_connection, "to input", this_index, last_midi_in_port_names[this_index])
                self.midi_input_reconnect_strings[this_connection] = last_midi_in_port_names[this_index]
                self.close_midi_in(this_connection)
                self.midi_input_indexes[this_connection] = self.MIDI_INDEX_NA
            #else:
            #    print this_index, "is not in dropped_input_indexes", dropped_input_indexes

        for this_connection in range(len(self.midi_output_indexes)):
            this_index = self.midi_output_indexes[this_connection]
            if this_index in dropped_output_indexes:
                print ("- Dropped Output Connection", this_connection, "to output", this_index, last_midi_out_port_names[this_index])
                self.midi_output_reconnect_strings[this_connection] = last_midi_out_port_names[this_index]
                self.close_midi_out(this_connection)
                self.midi_output_indexes[this_connection] = self.MIDI_INDEX_NA
            #else:
            #    print this_index, "is not in dropped_output_indexes", dropped_output_indexes

    # - Look for new ports that match ports that unexpectedly disconnected
    def identify_reattached_connections(self, added_input_indexes, added_output_indexes):
        for this_index in added_input_indexes:
            this_name = self.midi_in_port_names[this_index]
            if this_name in self.midi_input_reconnect_strings:
                print ("Detected desired reconnection. Reconnecting...")
                this_connection = self.midi_input_reconnect_strings.index(this_name)
                self.midi_input_reconnect_strings[this_connection] = ""
                self.open_midi_in(this_connection, this_index)
                if self.testing_enabled:
                    self.assign_midi_in_callback(this_connection, self.default_midi_callback)

        for this_index in added_output_indexes:
            this_name = self.midi_out_port_names[this_index]
            if this_name in self.midi_output_reconnect_strings:
                print ("Detected desired reconnection. Reconnecting...")
                this_connection = self.midi_output_reconnect_strings.index(this_name)
                self.midi_output_reconnect_strings[this_connection] = ""
                self.open_midi_out(this_connection, this_index)

    def __del__(self): # Routine that is run when the item is deleted
        pass

if __name__ == "__main__":
    def test_callback(event, data=None):
        print ("test_callback", event, data)
    
    import time
    midi_system = MIDISystemRt(testing_enabled=True)
    midi_system.open_midi_in(0,0)
    midi_system.open_midi_out(0,0)
    midi_system.open_midi_in(1,1)
    midi_system.open_midi_out(1,1)
    #midi_system.assign_midi_in_callback(0,test_callback)
  
    while (1):
        time.sleep(1)
        midi_system.refresh_midi_device_list()
        pass